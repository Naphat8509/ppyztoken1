"use client";
import React, { useState, useEffect } from "react";
import { initializeConnector } from "@web3-react/core";
import { MetaMask } from "@web3-react/metamask";

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';



import { ethers } from "ethers"
import {
  formatEther,
  parseUnits
} from "@ethersproject/units"
import abi from "./abi.json"

const [metaMask, hooks] = initializeConnector(
  actions => new MetaMask({ actions })
);
const { useChainId, useAccounts, useIsActivating, useIsActive, useProvider } =
  hooks;
const contractChain = 11155111;
const contractAddress = "0xe10d2804943121D9614E977d526DDB0a2200Ed65";



export default function Page() {
  const chainId = useChainId();
  const accounts = useAccounts();
  const isActive = useIsActive();

  const provider = useProvider();
  const [error, setError] = useState(undefined);


  const [balance, setBalance] = useState("");
  useEffect(() => {
    const fetchBalance=async()=>{
      const signer = provider.getSigner();
      const smartContract = new ethers.Contract(contractAddress, abi, signer)
      const myBalance = await smartContract.balanceOf(accounts[0])
      console.log(formatEther(myBalance))
      setBalance(formatEther(myBalance))
    }
    if (isActive){
      fetchBalance();
    }
  }, [isActive]);


  const [ppyzValue, setPPYzValue] = useState(0);

  const handleSetPPYzValue = event => {
    setPPYzValue(event.target.value);
  };
  
  const handleBuy = async () => {
    try {
      if (ppyzValue <= 0) {
        return;
      }

      const signer = provider.getSigner();
      const smartContract = new ethers.Contract(contractAddress, abi, signer);
      const buyValue = parseUnits(ppyzValue.toString(), "ether");
      const tx = await smartContract.buy({
        value: buyValue.toString(),
      });
      console.log("Transaction hash:", tx.hash);
    } catch (err) {
      console.log(err);
    }
  };
   

  const handleSell = async () => {
    try {
      if (ppyzValue <= 0) {
        return;
      }
  
      const signer = provider.getSigner();
      const smartContract = new ethers.Contract(contractAddress, abi, signer);
      const sellValue = parseUnits(ppyzValue.toString(), "ether");
  
      // Assuming your contract has a 'sell' function, modify this accordingly if it's different
      const tx = await smartContract.sell(sellValue.toString());
  
      console.log("Transaction hash:", tx.hash);
    } catch (err) {
      console.log(err);
    }
  };


  useEffect(() => {
    void metaMask.connectEagerly().catch(() => {
      console.debug("Failed to connect  to metamask");
    });
  }, []);

  const handleConnect = () => {
    metaMask.activate(contractChain);
  };

  const handleDisconnect = () => {
    metaMask.resetState();
  };

  const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);

const card = (
  <React.Fragment>
    <CardContent>
      <Typography sx={{ fontSize: 20 }} color="text.secondary" gutterBottom>
        My wallet balance
      </Typography>
    </CardContent>
  </React.Fragment>
);

  return (
    <div>
    <Box sx={{ flexGrow: 1 }}>
  <AppBar position="static" sx={{backgroundColor : "#49c3de"}}>
   <Toolbar>
  <IconButton
    size="large"
    edge="start"
    color="inherit"
    aria-label="menu"
    sx={{ mr: 2 }}
  >
    <MenuIcon />
  </IconButton>
 <img
              src = "https://i.pinimg.com/originals/7d/43/df/7d43dff976c4feff10e04c13a685bf62.gif" // URL ของรูปภาพโลโก้
              alt="PPYzToken Logo"
              style={{ height: 48, width: "auto" }} // ปรับขนาดตามความต้องการ
/>
  <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
    PPYzToken
  </Typography>
  {isActive ? (
    <Stack direction="row" spacing={1} alignItems="center">
      <Chip label={accounts ? accounts[0] : ""}/>
      <Button color="inherit" onClick={handleDisconnect}>
        Disconnect
      </Button>
    </Stack>
  ) : (
    <Button color="inherit" onClick={handleConnect}>
      Connect
    </Button>
     )}
  </Toolbar>
</AppBar>
  <br></br>
  <div>
   {isActive && (
    <Container maxWidth="1000px" sx={{ mt: 2 }}> 
    < Box sx = {
      {
        bgcolor: '#FFFAFA',
        height: '800px',
        width: 'pm',
        padding: '100px'
      }
    } >
    <Box sx={{ minWidth: 275 }}>
      <Card variant="outlined">{card}
        <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '50ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          required
          id="outlined-required"
          label="Address"
          value={accounts[0]}
          variant="outlined"
          InputProps = {
            {
              readOnly: true,
            }
          }
        />
      </div>
    </Box>

     <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '50ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          required
          id="outlined-required"
          label="PPYz Balance"
          value={balance}
          variant="outlined"
          InputProps = {
            {
              readOnly: true,
            }
          }
        />
      </div>
    </Box>
    <Typography
                  sx={{ fontSize: 20 }}
                  color="text.secondary"
                  gutterBottom
                >
                  Buy PPYz Token
                </Typography>
 <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '50ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          required
          id="outlined-required"
          label="Enter amount of Ether you to want to PPYz Token"
          defaultValue=""
            type = "number"
            onChange = {
              handleSetPPYzValue
            }
        />
      </div>
    </Box>
      <Button variant="contained" onClick={handleBuy}>
                  Buy Token
      </Button>

      <Typography
                  sx={{ fontSize: 20 }}
                  color="text.secondary"
                  gutterBottom
                >
                  Sell PPYz Token
                </Typography>
 <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '50ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          required
          id="outlined-required"
          label="Enter amount of Ether you to want to PPYz Token"
          defaultValue=""
            type = "number"
            onChange = {
              handleSetPPYzValue
            }
        />
      </div>
    </Box>
      <Button variant="contained" onClick={handleSell}>
                  Sell PPYzToken
      </Button>
      </Card>
    </Box>
    </Box> 
    </Container>
   )} 
  </div>

</Box>
</div>
  );
}